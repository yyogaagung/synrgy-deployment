import Footer from './component/Footer';
import Hero from './component/Hero';
import MainContent from './component/MainContent';

function App() {
  return (
    <div className="App">
      <Hero/>
      <MainContent/>
      <Footer/>
    </div>
  );
}

export default App;
