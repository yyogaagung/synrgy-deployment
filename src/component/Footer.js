import React, { Fragment } from "react";
import facebook from '../images/Facebook Icon.png';
import github from '../images/GitHub Icon.png';
import instagram from '../images/Instagram Icon.png';
import twitter from '../images/Twitter Icon.png';

export default function Footer(){
    return(
        <Fragment>
            <div className="footer--wrap">
                <ul>
                    <li>
                        <img
                            src={facebook}
                            alt='icon facebook'
                        />
                    </li>
                    <li>
                        <img
                            src={github}
                            alt='github icon'
                        />
                    </li>
                    <li>
                        <img
                            src={instagram}
                            alt='instagram icon'
                        />
                    </li>
                    <li>
                        <img
                            src={twitter}
                            alt='twitter icon'
                        />
                    </li>
                </ul>
            </div>
        </Fragment>
    )
}