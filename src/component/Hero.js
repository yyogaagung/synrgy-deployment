import React, { Fragment } from "react";
import profileImg from '../images/profile.jpeg'

export default function Hero(){
    return(
        <Fragment>
            <div className="wrap--hero">
                <img
                src={profileImg}
                alt="Profile"
                >
                </img>
            </div>
        </Fragment>
    )
}