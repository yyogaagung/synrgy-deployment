import React, { Fragment } from "react";
import mail from '../images/mail.png'
import linkedin from '../images/linkedin.png'

export default function MainContent(){
    return(
        <Fragment>
            <div className="biodata--wrap">
                <h2 className="biodata--name">Yoga Agung Prasetia</h2>
                <p className="biodata--role">Frontend Developer</p>
                <p className="biodata--education">SYNRGY Academy Batch 4</p>
            </div>

            <div className="contact--wrap">
                <ul>
                    <li className="contact--email">
                        <a href="/">
                            <img
                            className="contact--img"
                            src={mail}
                            alt="icon email">
                            </img>
                            <span >Email</span>
                        </a>
                    </li>  
                    <li className="contact--linkedin">
                        <a href="/">
                            <img
                            className="contact--img"
                            src={linkedin}
                            alt="icon email">
                            </img>
                            <span className="linkedin">LinkedIn</span>
                        </a>
                    </li>  
                </ul>
            </div>

            <div className="desc--wrap">
                <div className="desc--about">
                    <h4>About</h4>
                    <p>I am a frontend developer with a particular interest in making things simple and automating daily tasks. I try to keep up with security and best practices, and am always looking for new things to learn.</p>
                </div>
                <div className="desc--interests">
                    <h4>Interests</h4>
                    <p>Food expert. Music scholar. Reader. Internet fanatic. Bacon buff. Entrepreneur. Travel geek. Pop culture ninja. Coffee fanatic.</p>
                </div>
            </div>
        </Fragment>
    )
}